import random

artifact_desc_header = [
    'where I ',
    "in which I ",
]


difficulty = [
    "This was a pretty straightforward and painless task for me",
    "This was fairly easy for me, so I didn't have too much trouble with it",
    "Although not too difficult, a few parts of this tripped me up",
    "I was actually rather challenged while working on this",
    "This gave me quite a bit of trouble",
]
easy_difficulty = [
    "The only thing that was tough was ",
    "The only thing that tripped me up was ",
    "The only thing I struggled with was ",
]
harder_difficulty = [
    "I mainly struggled with ",
    "This was tough, since ",
]



futuretion_options = [
    "If I could do the assignment again, I would ",
    "In the future, I will ",
    "Moving forward, I will ",
]


concluders = [
    "In general, ",
    "Overall, ",
    "For the most part, ",
    "Altogether, ",
    "Mostly, though, ",
    "Generally, ",
]
conclusions = [
    "I liked this a lot, ",
    "I quite liked this, ",
    "I didn't enjoy this, ",
    "I thought that this was rather unenjoyable, ",
]
contrasting_conjugations = [
    "although ",
    "even though ",
    "but ",
    "however, ",
    "though ",
    "despite ",
]
conjunctions = [
    "since ",
    "because ",
    "as ",
]

def main():
    s_count = 0

    artifact = input('what is the artifact: ')
    artifact_desc = input('what did you do for the artifact?...I ')
    s_count += 1

    diff_int = int(input("how difficult was the artifact, (1:easiest, 5:hardest) : "))
    s_count += 1

    if (diff_int <= 2):
        contr_conj = easy_difficulty[random.randrange(0, len(easy_difficulty))] + input("the only thing difficult was... ")
    else:
        contr_conj = harder_difficulty[random.randrange(0, len(harder_difficulty))] + input("I mainly struggled with... ")
    s_count += 1

    learning_headers = [
        f"This {artifact} taught me ",
        f"By completing this {artifact}, I learned " ,
        f"Finishing up this {artifact} showed me ",
        f"From this {artifact}, I learned "
    ]
    learning = learning_headers[random.randrange(0, len(learning_headers))] + input("I learned... ")
    s_count += 1

    mid_sentence = input("talk about how you learned or practice some skills... ")
    s_count += 1

    futuretion_int = int(input("what you would do if you could do it over (1) or how you'll use the knowledge in the future (2): "))
    futuretion = futuretion_options[futuretion_int-1] + input(futuretion_options[futuretion_int-1] + '... ')
    s_count += 1

    like_int = int(input("""how much did you like the artifact:\n1: I liked this a lot,\n2: I quite liked this,\n3: I didn't enjoy this,\n
        4: I thought that this was rather unenjoyable,\n"""))
    ation_int = int(input("add explanation (1) or contradiction (2): "))
    if ation_int == 2:
        ation = contrasting_conjugations[random.randrange(0, len(contrasting_conjugations))]
    elif ation_int == 1:
        ation = conjunctions[random.randrange(0, len(conjunctions))]
    concl_end = ation + input(conclusions[like_int-1] + ation + "... ")
    conclusion = concluders[random.randrange(0, len(concluders))] + conclusions[like_int-1] + concl_end
    s_count += 1
    

    reflection = f"""
    This is my {artifact}, {artifact_desc_header[random.randrange(0, len(artifact_desc_header))]} {artifact_desc}. 
    {difficulty[diff_int-1]}. 
    {contr_conj}. 
    {learning}. 
    {mid_sentence}.
    {futuretion}. 
    {conclusion}.
    """
    print("\n" + reflection)


main()